## Functions for Shiny App (see also ../scratch.R)
library(tibble)

load_db <- function(db_path) {
    read_csv(db_path, header=TRUE)
}

subset_tracks_by_date <- function(df, date) {
    df  |>
        filter(date == date)
}

load_gps <- function(gpx_file_path) {
    do.call(rbind, gpx::read_gpx(gpx_file_path)$tracks) |>
        as_tibble() |>
        rename(Speed = speed) |>
        select(-c(`Segment ID`))
}

extract_metadata <- function(gpx_file_path) {
    track_gps <- load_gps(gpx_file_path)
    print(track_gps)
    GPX <- xml2::read_xml(gpx_file_path) |> xml_children() |> xml_children()
    print(GPX)
    start_time <- min(track_gps[["Time"]])
    finish_time <- max(track_gps[["Time"]])
    activity <- xml_text(GPX[3], trim = TRUE)
    INFO <- xml_children(GPX)
    track_id <- xml_text(INFO[2], trim = TRUE)
    STATS <- xml_children(INFO[3])
    distance <- xml_double(STATS[1])
    time_total <- xml_double(STATS[2])
    time_moving <- xml_double(STATS[3])
    time_stationary <- xml_double(STATS[4])
    max_speed <- xml_double(STATS[5])
    ascent <- xml_double(STATS[6])
    descent <- xml_double(STATS[7])
    total_gps <- nrow(track_gps)
    start_lat <- track_gps[["Latitude"]][1]
    start_lon <- track_gps[["Longitude"]][1]
    finish_lat <-track_gps[["Latitude"]][total_gps]
    finish_lon <- track_gps[["Longitude"]][total_gps]
    ## elevationChange <- track_gps[["Elevation"]] - c(track_gps[["Elevation"]][-1], NA)
    ## ascent <- sum(elevationChange[elevationChange > 0.0], na.rm = TRUE)
    ## descent <- sum(elevationChange[elevationChange < 0.0], na.rm = TRUE)
    summary_df <- c(track_id,
                    start_time,
                    finish_time,
                    activity,
                    as.numeric(distance),
                    as.numeric(time_total),
                    as.numeric(time_moving),
                    as.numeric(time_stationary),
                    as.numeric(max_speed),
                    as.numeric(total_gps),
                    as.numeric(start_lat),
                    as.numeric(start_lon),
                    as.numeric(finish_lat),
                    as.numeric(finish_lon),
                    as.numeric(ascent),
                    as.numeric(descent)) |>
        t() |>
        as_tibble()
    names(summary_df) <- c("track_id",
                           "start_time", "finish_time",
                           "activity",
                           "distance",
                           "time_total", "time_moving", "time_stationary",
                           "max_speed",
                           "total_gps",
                           "start_lat", "start_lon",
                           "finish_lat", "finish_lon",
                           "ascent", "descent")
    return(summary_df)
}

extract_track_stats <- function(track_gpx) {
}

append_stats <- function(df, track_stats) {
}

check_if_track_exists <- function(df, track_hash) {
}

plot_track <- function(track_gpx) {
    leaflet(track_gpx$gps) |>
        addTiles() |>
        addAwesomeMarkers(lat=self$start_loc[1],
                          lng=self$start_loc[2],
                          popup=self$start_popup()) |>
        addAwesomeMarkers(lat=self$finish_loc[1],
                          lng=self$finish_loc[2],
                          popup=self$finish_popup()) |>
        addPolylines(lng=self$gps$Longitude,
                     lat=self$gps$Latitude)
                     ## color=~Speed)

}

popup_start <- function(df, track_hash) {
    track <- df  |>
               filter(hash == track_hash)
    paste(sep="<br/>",
          paste0("<b>Start</b> : ", track_gpx$start_time),
          paste0("Altitude     : ", track_gpx$gps$Elevation[1]))
}

popup_finish <- function(track_gpx) {
    paste(sep="<br/>",
          paste0("<b>Finish</b>   : ", track_gpx$finish_time),
          paste0("Altitude        : ", track_gpx$gps$Elevation[1]),
          paste0("Distance (km)   : ", track_gpx$distance / 1000),
          paste0("Total Time      : ", track_gpx$time_total),
          paste0("Moving Time     : ", track_gpx$time_moving),
          paste0("Stationary Time : ", track_gpx$time_stationary))
}
