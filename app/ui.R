library(shiny)
library(shinythemes)
library(shinyFiles)
library(shiny.fluent)
library(shinyjs)
sidebar_width <- 4

ui <- fluidPage(
##    useShinyjs(),
    navbarPage("OpenTrackeR", theme = shinytheme("lumen"),
               tabPanel("Journey Selection",
                        sidebarLayout(
                            sidebarPanel(
                                width = sidebar_width,
                                titlePanel("Select Day"),
                                fluidRow(
                                    column(12,
                                           Calendar.shinyInput("date_to_view", "2020-06-25T22:00:00.000Z",
                                                               yearPickerHidden = 1)
                                           ),
                                    ## column(12,
                                    ##        selectInput(inputId = "track",
                                    ##                    label = "Select Track",
                                    ##     # Choices to come from selected day
                                    ##                    choices =  c("2022-03-09", "2022-03-08"),
                                    ##                    # choices = output$tracks_day,
                                    ##                    selected = "2022-03-09"
                                    ##                    )
                                    ##        ),
                                    column(12,
                                           conditionalPanel(
                                               condition = "exists('output.select_track')",
                                               uiOutput("select_track")
                                           )),
                                    column(12,
                                           selectInput(inputId = "activity",
                                                       label = "Select Activity",
                                                       # Choices to come from selected day
                                                       choices =  c("Running" = "running",
                                                                    "Cycling" = "cycling",
                                                                    "Hiking" = "hiking"),
                                                       selected = "Running")
                                    ),
                                    column(width = (12-sidebar_width),
                                           leafletOutput("map")
                                    )
                                )
                            ),
                            mainPanel(
                                width = (12 - sidebar_width),
                                column(width = (12-sidebar_width),
                                       p("Plot the journey here with time-series of speed/acceleration underneath."),
                                       textOutput("selected_date"),
                                       textOutput("selected_track"),
                                       textOutput("selected_full_path")
                                       ## textOutput("tracks")## ,
                                       ## leafletOutput("leaflet_plot")
                                       ## textOutput("whatdidweselect"),
                                       ## textOutput("selected_date")
                                       ## tableOutput("metadata")
                                       ## tabsetPanel(
                                       ##     tabPanel("Journey",
                                       ##              "The selected track will be shown below",
                                       ##              textOutput("calendarValue")
                                       ##              ),
                                       ##     tabPanel("Monthly",
                                       ##              "We will summarise stats by month here",
                                       ##              plotOutput("hist")
                                       ##              ),
                                       ##     tabPanel("Weekly",
                                       ##              "We will summarise stats by week here.",
                                       ##              plotOutput("hist")
                                       ##              )
                                       ## )
                                       )

                            )
                        )
                        ),
               tabPanel("Time-Series", fluid = TRUE,
                        titlePanel("Time-Series"),
                        fluidRow(
                            column(6,
                                   selectInput(inputId = "activity",
                                               label = "Select Activity",
                                        # Choices to come from selected day
                                               choices =  c("Running" = "running",
                                                            "Cycling" = "cycling",
                                                            "Hiking" = "hiking"),
                                               selected = "Running")
                            )
                        )
               ),
               tabPanel("Monthly Stats", fluid = TRUE,
                        titlePanel("Monthly Stats"),
                        fluidRow(
                            column(6,
                                   selectInput(inputId = "activity",
                                               label = "Select Activity",
                                        # Choices to come from selected day
                                               choices =  c("Running" = "running",
                                                            "Cycling" = "cycling",
                                                            "Hiking" = "hiking"),
                                               selected = "Running")
                            )
                        )
               ),
               tabPanel("Weekly Stats", fluid = TRUE,
                        titlePanel("Weekly Stats"),
                        fluidRow(
                            column(6,
                                   selectInput(inputId = "activity",
                                               label = "Select Activity",
                                        # Choices to come from selected day
                                               choices =  c("Running" = "running",
                                                            "Cycling" = "cycling",
                                                            "Hiking" = "hiking"),
                                               selected = "Running")
                            )
                        )
               )
    )
)
