library(leaflet)
library(R6)
library(tidyverse)

## Parse data using xml2 and converting to list to parse/extract.
## Heavily based on approach documented by Matt Dray https://www.rostrum.blog/2021/12/30/gpx3d/
## See code in https://github.com/matt-dray/gpx3d/
## I couldn't extract elements using Xpath approach?


## Parsing available files
## Get data on date/time of tracks
get_tracks_paths <- function(gpx_dir) {
    dir(gpx_dir, pattern = "*.gpx", full.names = TRUE) |>
        as_tibble_col(column_name = "full_path")
}
extract_track_file_name <- function(df) {
    mutate(df,
           file_name = stringr::str_extract(full_path, "//.*\\.gpx"),
           file_name = stringr::str_replace(file_name, "//", ""))
}
clean_file_name <- function(df) {
    mutate(df,
           clean_file_name = gsub("T", " ",
                                  gsub("Z", "", file_name)))
}
extract_date_time <- function(df) {
    mutate(df,
           date_str = stringr::str_extract(clean_file_name, "[0-9]{4}-[0-9]{2}-[0-9]{2}"),
           time = stringr::str_extract_all(clean_file_name, " [0-9]{2}_[0-9]{2}"),
           time = stringr::str_replace(time, "\\.gpx", ""),
           time = stringr::str_replace(time, "_", ":"))
}
convert_date_time <- function(df) {
    mutate(df,
           date_time = lubridate::ymd_hm(paste0(date_str, " ", time)),
           date = lubridate::ymd(date_str))
}
remove_variables <- function(df) {
    select(df, -date_str, -clean_file_name, -time)
}

get_all_gpx_files <- function(gpx_dir = GPXDIR) {
    get_tracks_paths(gpx_dir) |>
        extract_track_file_name() |>
        clean_file_name() |>
        extract_date_time() |>
        convert_date_time() |>
        remove_variables() |>
        arrange(date_time)## |>
        ## column_to_rownames(var = "file_name")
}
todays_tracks <- function(gpx_dir = GPXDIR, selected_date = NA) {
    if(is.na(selected_date)) {
        selected_date <- Sys.Date()
    }
    ## tracks <-
    get_all_gpx_files(gpx_dir) |>
        dplyr::filter(date == selected_date) |>
        dplyr::select(full_path) ## , full_path)  |>
        ## dplyr::select(file_name) ## , full_path)  |>
        ## as.list()
    ## tracks <- setNames(as.character(tracks$full_path), tracks$file_name)
    ## return(tracks)
}
## todays_tracks()

## get_all_gpx_files()


## Helper Functions
.gps_df <- function(trkseg) {
    trkseg_attrs <-  attributes(trkseg)
    lat <- as.numeric(trkseg_attrs[["lat"]])
    lon <- as.numeric(trkseg_attrs[["lon"]])
    time <- strptime(trkseg[["time"]][[1]], "%Y-%m-%dT%H:%M:%OSZ")
    if(is.na(time)) {
        time <- str_replace(trkseg[["time"]][[1]], "\\+01:00", "Z") |>
            strptime("%Y-%m-%dT%H:%M:%OSZ")
    }
    ele <- as.numeric(trkseg[["ele"]][[1]])
    speed <- as.numeric(trkseg$extensions$TrackPointExtension$speed)
    if( length(speed) > 0 ) {
        data.frame(
            Latitude  = lat,
            Longitude = lon,
            Time      = time,
            Elevation = ele,
            Speed     = speed
        )
    }
}

.extract_gps <- function(trkseg) {
    route_list <- lapply(trkseg, function(x) .gps_df(x))
    do.call(rbind, route_list)
}


## Class for processing a single track
OpenTracks <- R6Class("OpenTracks",
                      list(filename = NULL,
                           gps = NULL,
                           activity = NULL,
                           track_id = NULL,
                           start_time = NULL,
                           finish_time = NULL,
                           time_total = NULL,
                           time_moving = NULL,
                           time_stationary = NULL,
                           distance = NULL,
                           ascent = NULL,
                           descent = NULL,
                           max_speed = NULL,
                           total_gps = NULL,
                           start_lat= NULL,
                           start_lon= NULL,
                           finish_lat = NULL,
                           finish_lon = NULL,
                           summary_df = NULL,
                           segments = NULL,
                           gpx_xml = NULL,
                           gpx_list = NULL,
                           initialize = function(filename) {
                               print(filename)
                               stopifnot(file.exists(filename))
                               self$filename <- filename
                               self$load_xml()
                           },
                           print = function(...) {
                               cat("Track ID         : ", self$track_id, "\n", sep = "")
                               cat("Activity         : ", self$activity, "\n", sep = "")
                               cat("Start Date/Time  : ", self$start_time, "\n", sep = "")
                               cat("Finish Date/Time : ", self$finish_time, "\n", sep = "")
                               cat("Segments         : ", self$segments, "\n", sep = "")
                               cat("Distance (m)     : ", self$distance, "\n", sep = "")
                               cat("Distance (km)    : ", self$distance / 1000, "\n", sep = "")
                               cat("Time Total       : ", self$time_total, "\n", sep = "")
                               cat("Time Moving      : ", self$time_moving, "\n", sep = "")
                               cat("Time Stationary  : ", self$time_stationary, "\n", sep = "")
                               cat("Max Speed        : ", self$max_speed, "\n", sep = "")
                               cat("Ascent           : ", self$ascent, "\n", sep = "")
                               cat("Descent          : ", self$descent, "\n", sep = "")
                               cat("Start Lat/Lon    : ", paste0(self$start_lat, ",", self$start_lon), "\n", sep = "")
                               cat("Finish Lat/Lon   : ", paste0(self$finish_lat, ",", self$finish_lon), "\n", sep = "")
                           },
                           validate = function(...) {
                               # TODO : Bulk out validation of data types
                               stopifnot(is.numeric(self$max_speed))
                           },
                           calculate_distance = function(...) {
                               self$gps$Distance <- haversine(c(self$gps$Latitude[-1], NA),
                                                              c(self$gps$Longitude[-1], NA),
                                                              self$gps$Latitude,
                                                              self$gps$Longitude)
                           },
                           ## Read track using XML2
                           load_xml = function(...) {
                               self$gpx_xml <- xml2::read_xml(self$filename)
                               self$gpx_list <- xml2::as_list(self$gpx_xml)
                           },
                           count_segments = function(...) {
                               self$segments <- length(self$gpx_list$gpx$trk) - 4
                           },
                           extract_segments = function(...) {
                               segments_list <- lapply(self$gpx_list$gpx$trk[5:(self$segments + 4)],
                                                      function(x) .extract_gps(x))
                               self$gps <- do.call(rbind, segments_list)
                               rownames(self$gps) <- NULL
                           },
                           extract_metadata = function(...) {
                               self$start_time <- min(self$gps[["Time"]])
                               self$finish_time <- max(self$gps[["Time"]])
                               self$activity <- self$gpx_list$gpx$trk$type[[1]]
                               self$track_id <- self$gpx_list$gpx$trk$extensions$trackid[[1]]
                               self$distance <- as.numeric(self$gpx_list$gpx$trk$extensions$TrackStatsExtension$Distance[[1]])
                               self$time_total <- as.numeric(self$gpx_list$gpx$trk$extensions$TrackStatsExtension$TimerTime[[1]])
                               self$time_moving <- as.numeric(self$gpx_list$gpx$trk$extensions$TrackStatsExtension$MovingTime[[1]])
                               self$time_stationary <- as.numeric(self$gpx_list$gpx$trk$extensions$TrackStatsExtension$StoppedTime[[1]])
                               self$max_speed <- as.numeric(self$gpx_list$gpx$trk$extensions$TrackStatsExtension$MaxSpeed[[1]])
                               self$ascent <- as.numeric(self$gpx_list$gpx$trk$extensions$TrackStatsExtension$Ascent[[1]])
                               self$descent <- as.numeric(self$gpx_list$gpx$trk$extensions$TrackStatsExtension$Descent[[1]])
                               self$total_gps <- nrow(self$gps)
                               self$start_lat <- self$gps$Latitude[1]
                               self$start_lon <- self$gps$Longitude[1]
                               self$finish_lat <-self$gps$Latitude[self$total_gps]
                               self$finish_lon <- self$gps$Longitude[self$total_gps]
                               elevationChange <- self$gps$Elevation - c(self$gps$Elevation[-1], NA)
                               self$ascent <- sum(elevationChange[elevationChange > 0.0], na.rm = TRUE)
                               self$descent <- sum(elevationChange[elevationChange < 0.0], na.rm = TRUE)
                           },
                           summarise = function(...) {
                               self$summary_df <- data.frame(track_id = self$track_id,
                                                             start_time = self$start_time,
                                                             finish_time = self$finish_time,
                                                             activity = self$activity,
                                                             distance = self$distance,
                                                             time_total = self$time_total,
                                                             time_moving = self$time_moving,
                                                             time_stationary = self$time_stationary,
                                                             max_speed = self$max_speed,
                                                             total_gps = self$total_gps,
                                                             start_lat = self$start_lat,
                                                             start_lon = self$start_lon,
                                                             finish_lat = self$finish_lat,
                                                             finish_lon = self$finish_lon,
                                                             ascent = self$ascent,
                                                             descent = self$descent)
                               self$summary_df <- self$summary_df |>
                               mutate(start_time = as.numeric(start_time),
                                      start_time = as.POSIXct(start_time, origin="1970-01-01"),
                                      finish_time = as.numeric(finish_time),
                                      finish_time = as.POSIXct(finish_time, origin="1970-01-01"),
                                      distance = as.numeric(distance),
                                      distance_km = distance / 1000,
                                      time_total = as.numeric(time_total),
                                      time_moving = as.numeric(time_moving),
                                      time_stationary = as.numeric(time_stationary),
                                      pace_km = time_moving / distance_km,
                                      speed_kmh = distance_km / (time_moving / 3600),
                                      max_speed = as.numeric(max_speed),
                                      total_gps = as.numeric(total_gps),
                                      start_lat = as.numeric(start_lat),
                                      start_lon = as.numeric(start_lon),
                                      finish_lat = as.numeric(finish_lat),
                                      finish_lon = as.numeric(finish_lon),
                                      ascent = as.numeric(ascent),
                                      descent = as.numeric(descent),
                                      ) |>
                               as_tibble()
                           },
                           start_popup = function(...) {
                               paste(sep="<br/>",
                                     paste0("<b>Start</b> : ", self$start_time),
                                     paste0("Altitude     : ", self$gps$Elevation[1]))
                           },
                           finish_popup = function(...) {
                               paste(sep="<br/>",
                                     paste0("<b>Finish</b>   : ", self$finish_time),
                                     paste0("Altitude        : ", self$gps$Elevation[1]),
                                     paste0("Distance (km)   : ", self$distance / 1000),
                                     paste0("Total Time      : ", self$time_total),
                                     paste0("Moving Time     : ", self$time_moving),
                                     paste0("Stationary Time : ", self$time_stationary))
                           },
                           color_gradient = function(low='green', high='red', ...) {
                               gradient_function <- colorRampPalette(c(low, high), method="linear")
                               gradientFunction(dim(self$gps))
                           },
                           plot_track = function(...) {
                               leaflet(self$gps) |>
                                   addTiles() |>
                                   addAwesomeMarkers(lat=self$start_lat,
                                                     lng=self$start_lon,
                                                     popup=self$start_popup()) |>
                                   addAwesomeMarkers(lat=self$finish_lat,
                                                     lng=self$finish_lon,
                                                     popup=self$finish_popup()) |>
                                   addPolylines(lng=self$gps$Longitude,
                                                lat=self$gps$Latitude)
                                        # color=~Speed)
                           },
                           plot_speed = function(...) {
                           },
                           plot_acceleration = function(...) {
                           }
                           ))
process_opentracks <- function(file_path, info = FALSE, plot = FALSE, return_summary = FALSE) {
    open_track <- OpenTracks$new(file_path)
    open_track$load_xml()
    open_track$count_segments()
    open_track$extract_segments()
    open_track$extract_metadata()
    return(open_track)
}

## opentrack_today <- process_opentracks(gpx_today$full_path, plot=TRUE)
## print(opentrack_today)
## opentrack_today$
